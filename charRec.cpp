/*
 * Christian Weigandt
 * Character Recognizer
 * March 2013
 * 
 * Uses OpenGL along with the LeapMotion framework to allow drawing on screen
 *
 * TODO - Incorporate neural network to actually recognize digits
 *      - Test resize function
 *      - Would also be nice to recognize pinch gesture but who knows how to do that
 *      - Eventually move logging to a file to be cleaner
 *      - Do something about the plethora of include files
 *
 */


//--IO includes--
#include <iostream>
using namespace std;
//--Stdlib includes--
#include <list>
#include <stddef.h>
//--Math includes--
#include <math.h>
//--Leap includes--
#include "Leap.h"
using namespace Leap;
//--OPENGL includes--
#ifdef __APPLE__
#  include <OpenGL/gl.h>
#  include <OpenGL/glu.h>
#  include <GLUT/glut.h>
#else
#  include <GL/gl.h>
#  include <GL/glu.h>
#  include <GL/glut.h>
#endif

//Prototypes
void init();
void cleanUpandKill();
void myResize(GLint w, GLint h);
void display();
void keyboardFunc(unsigned char key, int x, int y);
void update();
void handleGesture(Gesture gesture);
void printFrame(const Frame frame);

//Not really used -- used in resizeFunc
int baseLength = 300;
//Vector is Leap::Vector
list<Vector> points;
list<Vector>::iterator itPoints;
Vector curPoint;
//Flag for drawing
bool shouldDraw = false;


//Print frame information
//Kept in case we need examples of how to get information
void printFrame(const Frame frame){
   cout << "Frame id: " << frame.id()
            << ", timestamp: " << frame.timestamp()
            << ", hands: " << frame.hands().count()
            << ", fingers: " << frame.fingers().count()
            << ", tools: " << frame.tools().count()
            << ", gestures: " << frame.gestures().count() << endl;
}

//Interprets gestures from Leap Device
void handleGesture(Gesture gesture){
    switch (gesture.type()) {
      case Gesture::TYPE_SWIPE:
      {
         SwipeGesture swipe = gesture;
         Vector dir = swipe.direction();
         if(dir.x < 0 && abs(dir.x) > abs(dir.y)){
            points.erase(points.begin(), points.end());
         }
         break;
      }
      case Gesture::TYPE_SCREEN_TAP:
      {
         cout<<"TAPPED"<<endl;
         //Could be nice if a tap signified 'enter'
         break;
      }
      default:
         cout << "Unknown gesture type." << endl;
         break;
    }
}

//--- Leap Listener Functions ---
class SampleListener : public Listener {
   public:
    virtual void onInit(const Controller&);
    virtual void onConnect(const Controller&);
    virtual void onDisconnect(const Controller&);
    virtual void onExit(const Controller&);
    virtual void onFrame(const Controller&);
};

void SampleListener::onInit(const Controller& controller) {
   cout << "Initialized" << endl;
}

void SampleListener::onConnect(const Controller& controller) {
   cout << "Connected" << endl;
   controller.enableGesture(Gesture::TYPE_SWIPE);
   controller.enableGesture(Gesture::TYPE_SCREEN_TAP);
}

void SampleListener::onDisconnect(const Controller& controller) {
   cout << "Disconnected" << endl;
}

void SampleListener::onExit(const Controller& controller) {
   /*would love to have this work*/
   //controller.removeListener(*this);
   cout << "Exited" << endl;
}

//The important Listener function
void SampleListener::onFrame(const Controller& controller) {
   const Frame frame = controller.frame();
//   printFrame(frame);
   if (!frame.hands().empty()) {
      const Hand hand = frame.hands()[0];
      const FingerList fingers = hand.fingers();
      if (fingers.count() == 1){ //Only draw when one finger is showing
         curPoint = fingers[0].tipPosition();
         //cout<<curPoint<<endl;
         if(shouldDraw){
            points.push_back(curPoint);
         }
      }
   }
   
   if(!frame.gestures().empty())
      handleGesture(frame.gestures()[0]);

}

//--- End Leap Listener Functions ---

void update(){
   glutPostRedisplay();
}

void keyboardFunc(unsigned char key, int x, int y){

   switch(key){
      case 'd': //Toggle drawing
         shouldDraw = !shouldDraw;
         points.push_back(Vector(FLT_MIN,FLT_MIN,FLT_MIN));
         break;
      case 'e': //Erase all points
         points.erase(points.begin(), points.end());
         break;
      case 'q': //Quit
         //glutLeaveMainLoop();
         cleanUpandKill();
         break;
   }

}

//-------------------
//DISPLAY FUNCTION
//JUMP - keyword for searching once the file gets huge
//use /JUMP in vi command mode
void display()
{
   glClear(GL_COLOR_BUFFER_BIT);

   //Draw cursor over finger
   glColor3f(0.4,0.4,0.4);
   glBegin(GL_POINTS);
      glVertex2f(curPoint.x,curPoint.y);
   glEnd();

   //Draw image
   glColor3f(0.0,0.0,0.0);
   glBegin(GL_LINE_STRIP);
      itPoints = points.begin();
      while(itPoints != points.end()){
         if((*itPoints).x != FLT_MIN)
            glVertex2f((*itPoints).x,(*itPoints).y);
         else{ //Drawing was toggled at this point so we end the current image and begin a new one
            glEnd();
            glBegin(GL_LINE_STRIP);
         }
         ++itPoints;
      }
   glEnd();

   glutSwapBuffers();
}
//-------------------

void myResize(GLint w, GLint h){

   glViewport(0,0,w,h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
//   gluOrtho2D(-120.0,120.0,80.0,320.0);
   gluOrtho2D(-(GLfloat)w/baseLength,(GLfloat)w/baseLength,-(GLfloat)h/baseLength,(GLfloat)h/baseLength);
   glMatrixMode(GL_MODELVIEW);
   
}

//Leap objects
//Defined down here so SampleListener class is predefined and because cleanUpandKill needs it
//Still looking for a better way to remove the listener
SampleListener mainListener;
Controller mainController;


//Kill everything
void cleanUpandKill(){
   cout<<"Quitting"<<endl;
   points.erase(points.begin(), points.end());
   mainController.removeListener(mainListener);
   exit(0);
}

//Initialize OpengGL parameters
void init(){

   glClearColor(0.8,0.8,0.8,1.0);
   glColor3f(0.0,0.0,0.0);
   glClear(GL_COLOR_BUFFER_BIT);
   gluOrtho2D(-120.0,120.0,80.0,320.0);
   glLineWidth(6);
   glPointSize(6);
   glEnable(GL_LINE_SMOOTH);

}

int main(int argc, char** argv) {
   // Create a sample listener and controller

   //Initialize GLUT
   glutInit(&argc, argv);
   glutInitWindowSize(600,600);
   glutCreateWindow("Character Recognizer");
   glutDisplayFunc(display);
//   glutReshapeFunc(myResize);  //Reshape function probably doesn't work yet
   glutIdleFunc(update);
   glutKeyboardFunc(keyboardFunc);
   init();

   // Have the sample listener receive events from the controller
   mainController.addListener(mainListener);

   glutMainLoop();

   return 0;
}
