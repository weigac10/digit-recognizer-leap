CC := g++
OPTIONS := -Wall -g
LEAPLIB := -Iinclude lib/libLeap.dylib
UNAME := $(shell uname)
FNAME := charRec
LDFLAGS	= -lGL -lglut -lGLU
ifeq ($(UNAME), Darwin)
        LDFLAGS = -framework OPENGL -framework GLUT -lm
endif

main: $(FNAME).cpp
	$(CC) $(OPTIONS) $(LEAPLIB) $(FNAME).cpp -o $(FNAME) $(LDFLAGS)
ifeq ($(UNAME), Darwin)
	install_name_tool -change @loader_path/libLeap.dylib lib/libLeap.dylib $(FNAME)
endif

clean:
	rm -rf $(FNAME) $(FNAME).dSYM
